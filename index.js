

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors"); 
const dotenv = require("dotenv").config();

//Routes

const userRoutes =require("./routes/userRoutes")

const productRoutes = require("./routes/productRoutes")

const cartRoutes = require("./routes/cartRoutes")

//Connect express

const app = express();
const port = 3060;
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));



mongoose.connect(`mongodb+srv://johnbenedictpalma:${process.env.PASSWORD}@cluster0.zxskweo.mongodb.net/e-commerce-api?retryWrites=true&w=majority`, {

    useNewUrlParser: true,
    useUnifiedTopology: true

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
);


let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("Connected to MongoDB.")

    )

app.use("/users", userRoutes);

app.use("/products", productRoutes);

app.use("/carts", cartRoutes);



app.listen(port, () => console.log(`API is now online on port ${port}`));