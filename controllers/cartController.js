const Product = require('../models/products');

const User = require('../models/users');

const Order = require("../models/cartModel")

const jwt = require('jsonwebtoken');

const auth = require("../auth");


//userId comes from the payload which is from auth.js


module.exports.mycheckout = async (req) => {

    

    let product = await Product.findById (req.body.product.productId)

    let newOrder = new Order ({

                userId: req.body.payload.id,
                products: [{

                    productId: req.body.product.productId,
                    quantity: req.body.product.quantity
                }],

                totalAmount: product.price * req.body.product.quantity       
            });

                return await newOrder.save()


    } 

