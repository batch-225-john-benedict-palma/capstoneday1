const Product = require("../models/products");


module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let new_product = new Product ({
			name: data.products.name,
			description: data.products.description,
			price: data.products.price
		})

		return new_product.save().then((new_product, error) => {
			if(error){
				return false
			}

			return {
				message: 'New product successfully created!'
			}
		})
	} 

	let message = Promise.resolve('User must be Admin to Access this.')

	return message.then((value) => {
		return value

	// you can use this to simplify code
	// Promise.resolve('User must me Admin to Access this.')
	})
		
}


module.exports.getProducts = (reqBody) => {

    return Product.find().then(result => {

        return result;
    })
}


module.exports.getOneProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then(result => {

        return result;
    })
}


module.exports.updateProducts = (reqParams, reqBody) => {

	let updatedProduct = {

		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};


	/*console.log(reqParams.productId);
	console.log(updatedProduct);*/

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then(result => {

        return result;
    });

};


module.exports.archiveProduct = (reqParams) => {


	let updateActiveField = {

		isActive : false
	};


	return Product.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		if (error) {

			return false;

		} else {

			return {

				message: "Product placed on archive successfully."
			}
		};
	});

};

