const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	cart: {
		items: [
			{
				productId: {
					type: mongoose.Schema.Types.ObjectId,
					ref: 'Product',
					required: true
				},
				quantity: {
					type: Number,
					required: true
				}
			}
		]
	},
	orders : [ 
		{
			products : [{ 
				productName : {
					type: String,
					required: [true, "Product is required"]
				},
				quantity: {
					type: Number,
					required: [true, "quantity is required"]
				}
			}],
			totalAmount: {
				type: Number,
				required: [true, "total amount is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date ()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);