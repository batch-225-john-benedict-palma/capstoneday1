const express = require("express");
const router = express.Router();

const auth = require("../auth")

const productController = require("../controllers/productController");


//Router for creating a course


router.post("/addProduct", auth.verify, (req, res) => {

	const data = {
		products : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(result => res.send (result));
});


router.get("/getall", (req, res) => {
    productController.getProducts(req.body).then(result => res.send(result));
})


router.get("/getone/:productId", (req, res) => {
    productController.getOneProduct(req.params).then(result => res.send(result)); 
})



router.put("/:productId", auth.verifyAdmin, (req, res) => {


//console.log("I am here")

	productController.updateProducts(req.params, req.body).then(result => res.send(result));
})



router.put("/archive/:courseId", auth.verifyAdmin, (req, res) => {

	productController.archiveProduct(req.params).then(result => res.send(result));
});

//-----


/*

router.put("update/:productId", auth.verify, (req, res) => {


//
	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
//

	productController.updateProducts2(data).then(result => res.send(result));
})
*/


module.exports = router;