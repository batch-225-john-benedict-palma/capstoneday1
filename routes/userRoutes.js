const express = require("express");
const router = express.Router();

const auth = require("../auth");

const userController = require("../controllers/userController")



router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(result => res.send(result));

});


router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(result => res.send(result))
});


router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(result => res.send(result))
})


router.get("/getUsers", (req, res) => {
    userController.getUsers(req.body).then(result => res.send(result));
})




module.exports = router;